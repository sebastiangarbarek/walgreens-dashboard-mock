//
//  StoreListController.m
//  WalgreensDashboardMock
//
//  Created by Sebastian Garbarek on 28/04/17.
//  Copyright © 2017 Sebastian Garbarek. All rights reserved.
//

#import "StoreListController.h"
#import "StoreNumberCell.h"
#import "StoreDetailViewController.h"

@interface StoreListController ()

@end

@implementation StoreListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_numberOfActiveStoresLabel setText:[NSString stringWithFormat:@"%lu Active Store(s):", [_storeNumbers count]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_storeNumbers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StoreNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Store Number Cell" forIndexPath:indexPath];
    
    // Get String value of NSNumber.
    NSString *storeNumber = [_storeNumbers[[indexPath row]] stringValue];
    // Set the label to store number.
    [[cell numberLabel] setText:storeNumber];
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Show Store Detail"]) {
        StoreDetailViewController *storeDetailViewController = [segue destinationViewController];
        
        NSIndexPath *selectedPath = [[self tableView] indexPathForSelectedRow];
        storeDetailViewController.selectedStoreNumber = _storeNumbers[[selectedPath row]];
    }
}

@end
