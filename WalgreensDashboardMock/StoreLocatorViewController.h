//
//  StoreLocatorViewController.h
//  WalgreensDashboardMock
//
//  Created by Sebastian Garbarek on 29/04/17.
//  Copyright © 2017 Sebastian Garbarek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreLocatorViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *latInputField;
@property (weak, nonatomic) IBOutlet UITextField *longInputField;
@property (weak, nonatomic) IBOutlet UIButton *locateButton;

@property (weak, nonatomic) IBOutlet UITextView *storeLocatorTextView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityIndicator;

@end
