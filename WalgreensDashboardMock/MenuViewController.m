//
//  TableViewController.m
//  WalgreensDashboardMock
//
//  Created by Sebastian Garbarek on 26/04/17.
//  Copyright © 2017 Sebastian Garbarek. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuItemCell.h"
#import "StoreListController.h"

@interface MenuViewController () {
    NSArray *menuItems;
    
    NSArray *storeNumbers;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems = @[@"Store List", @"Store Locator"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Menu Item" forIndexPath:indexPath];
    
    [[cell menuItemLabel] setText:menuItems[[indexPath row]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([menuItems[[indexPath row]] isEqualToString:@"Store List"]) {
        // Show network activity indicator.
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        // Create request data.
        NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionary];
        [requestDictionary setValue:@"HaVvNTNGKqsZuZR8ARAC0q3rvAeuam5P" forKey:@"apiKey"];
        [requestDictionary setValue:@"photoapi" forKey:@"affId"];
        [requestDictionary setValue:@"storeNumber" forKey:@"act"];
        [requestDictionary setValue:@"iPhone,10.3" forKey:@"devinf"];
        [requestDictionary setValue:@"1.00" forKey:@"appver"];
        
        // Convert dictionary to JSON representation for HTTP body.
        NSData *jsonRequestData = [NSJSONSerialization dataWithJSONObject:requestDictionary options:NSJSONWritingPrettyPrinted error:nil];
        
        // Setup HTTP request.
        NSURL *requestURL = [NSURL URLWithString:@"https://services-qa.walgreens.com/api/util/storenumber"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:requestURL];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:jsonRequestData];
        
        // Debug JSON request.
        NSLog(@"Request:\n%@", [[NSString alloc] initWithData:jsonRequestData encoding:NSASCIIStringEncoding]);
        
        // Send request.
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *jsonResponseData, NSURLResponse *responseURL, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
                NSDictionary *parsedResponse = [NSJSONSerialization JSONObjectWithData:jsonResponseData options:0 error:nil];
                storeNumbers = [parsedResponse valueForKey:@"store"];
                
                [self performSegueWithIdentifier:@"Display Store Numbers" sender:self];
            });
            
            // Debug JSON response.
            NSLog(@"Response:\n%@", [[NSString alloc] initWithData:jsonResponseData encoding:NSASCIIStringEncoding]);
        }] resume];
    } else if ([menuItems[[indexPath row]] isEqualToString:@"Store Locator"]) {
        [self performSegueWithIdentifier:@"Locate Stores" sender:self];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Display Store Numbers"]) {
        StoreListController *storeListController = [segue destinationViewController];
        storeListController.storeNumbers = storeNumbers;
    } else if ([[segue identifier] isEqualToString:@"Locate Stores"]) {
        // Go to.
    }
}

@end
