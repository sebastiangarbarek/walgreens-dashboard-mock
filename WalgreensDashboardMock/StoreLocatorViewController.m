//
//  StoreLocatorViewController.m
//  WalgreensDashboardMock
//
//  Created by Sebastian Garbarek on 29/04/17.
//  Copyright © 2017 Sebastian Garbarek. All rights reserved.
//

#import "StoreLocatorViewController.h"

@interface StoreLocatorViewController ()

@end

@implementation StoreLocatorViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
}

- (IBAction)sendLocationRequest:(id)sender {
    [_loadingActivityIndicator startAnimating];
    
    // Create request data.
    NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionary];
    [requestDictionary setValue:@"HaVvNTNGKqsZuZR8ARAC0q3rvAeuam5P" forKey:@"apiKey"];
    [requestDictionary setValue:@"photoapi" forKey:@"affId"];
    // Spoof location in the U.S.
    [requestDictionary setValue:[_latInputField text] forKey:@"lat"];
    [requestDictionary setValue:[_longInputField text] forKey:@"lng"];
    [requestDictionary setValue:@"" forKey:@"srchOpt"];
    [requestDictionary setValue:@"" forKey:@"nxtPrev"];
    [requestDictionary setValue:@"locator" forKey:@"requestType"];
    [requestDictionary setValue:@"fndStore" forKey:@"act"];
    [requestDictionary setValue:@"fndStoreJSON" forKey:@"view"];
    [requestDictionary setValue:@"iPhone,10.3" forKey:@"devinf"];
    [requestDictionary setValue:@"1.00" forKey:@"appver"];
    
    // Convert dictionary to JSON representation for HTTP body.
    NSData *jsonRequestData = [NSJSONSerialization dataWithJSONObject:requestDictionary options:NSJSONWritingPrettyPrinted error:nil];
    
    // Setup HTTP request.
    NSURL *requestURL = [NSURL URLWithString:@"https://services-qa.walgreens.com/api/stores/search"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:requestURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonRequestData];
    
    // Debug JSON request.
    NSLog(@"Request:\n%@", [[NSString alloc] initWithData:jsonRequestData encoding:NSASCIIStringEncoding]);
    
    // Send request.
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *jsonResponseData, NSURLResponse *responseURL, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_loadingActivityIndicator stopAnimating];
            
            // Debug JSON response.
            NSLog(@"Response:\n%@", [[NSString alloc] initWithData:jsonResponseData encoding:NSASCIIStringEncoding]);
            
            NSError *e;
            id prettyPrintedResponse = [NSJSONSerialization JSONObjectWithData:jsonResponseData options:0 error:&e];
            
            if (!prettyPrintedResponse) {
                // Format response for display.
                NSLog(@"Error parsing JSON: %@", e);
                [_storeLocatorTextView setText:@"No stores found."];
            } else {
                [_storeLocatorTextView setText:[[NSString alloc] initWithString:[prettyPrintedResponse description]]];
            }
        });
    }] resume];
}

// Dismiss keyboard on pressing return.
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

// End text field editing on touch of screen.
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
