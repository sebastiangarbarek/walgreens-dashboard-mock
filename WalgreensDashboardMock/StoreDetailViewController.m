//
//  StoreDetailViewController.m
//  WalgreensDashboardMock
//
//  Created by Sebastian Garbarek on 29/04/17.
//  Copyright © 2017 Sebastian Garbarek. All rights reserved.
//

#import "StoreDetailViewController.h"

@interface StoreDetailViewController ()

@end

@implementation StoreDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_loadingActivityIndicator startAnimating];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    // Create request data.
    NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionary];
    [requestDictionary setValue:@"HaVvNTNGKqsZuZR8ARAC0q3rvAeuam5P" forKey:@"apiKey"];
    [requestDictionary setValue:@"photoapi" forKey:@"affId"];
    [requestDictionary setValue:_selectedStoreNumber forKey:@"storeNo"];
    [requestDictionary setValue:@"storeDtl" forKey:@"act"];
    [requestDictionary setValue:@"storeDtlJSON" forKey:@"view"];
    [requestDictionary setValue:@"iPhone,10.3" forKey:@"devinf"];
    [requestDictionary setValue:@"1.00" forKey:@"appver"];
    
    // Convert dictionary to JSON representation for HTTP body.
    NSData *jsonRequestData = [NSJSONSerialization dataWithJSONObject:requestDictionary options:NSJSONWritingPrettyPrinted error:nil];
    
    // Setup HTTP request.
    NSURL *requestURL = [NSURL URLWithString:@"https://services-qa.walgreens.com/api/stores/details"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:requestURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonRequestData];
    
    // Debug JSON request.
    NSLog(@"Request:\n%@", [[NSString alloc] initWithData:jsonRequestData encoding:NSASCIIStringEncoding]);
    
    // Send request.
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *jsonResponseData, NSURLResponse *responseURL, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_loadingActivityIndicator stopAnimating];
            
            // Format response for display.
            id prettyPrintedResponse = [NSJSONSerialization JSONObjectWithData:jsonResponseData options:0 error:nil];
            
            [_storeDetailTextView setText:[[NSString alloc] initWithString:[prettyPrintedResponse description]]];
        });
        
        // Debug JSON response.
        NSLog(@"Response:\n%@", [[NSString alloc] initWithData:jsonResponseData encoding:NSASCIIStringEncoding]);
    }] resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
