//
//  StoreDetailViewController.h
//  WalgreensDashboardMock
//
//  Created by Sebastian Garbarek on 29/04/17.
//  Copyright © 2017 Sebastian Garbarek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreDetailViewController : UIViewController

@property (weak, nonatomic) NSString *selectedStoreNumber;

@property (weak, nonatomic) IBOutlet UITextView *storeDetailTextView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityIndicator;

@end
